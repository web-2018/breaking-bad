import {StyleSheet} from 'react-native';
import {colors} from '../../Helpers/colors';
// Home Screen Styles
export const styles = StyleSheet.create({
  flatListWrapperStyle: {
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 15,
    backgroundColor: colors.secondary,
  },
});
