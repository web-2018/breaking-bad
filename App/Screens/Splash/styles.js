import {StyleSheet} from 'react-native';
import {colors} from '../../Helpers/colors';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.black,
  },
});
export default styles;
