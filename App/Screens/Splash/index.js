import React, {useEffect} from 'react';
import {Text, View, Image} from 'react-native';
import {colors} from '../../Helpers/colors';
import {images} from '../../Helpers/images';
import styles from './styles';

const Splash = ({navigation: {replace}}) => {
  useEffect(() => {
    setTimeout(() => {
      replace('Home');
    }, 2000);
  }, []);
  return (
    <View style={styles.container}>
      <Image source={images.splashBanner} />
    </View>
  );
};

export default Splash;
