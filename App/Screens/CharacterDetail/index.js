import React from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  ScrollView,
  FlatList,
} from 'react-native';
import Header from '../../Components/Header';
import OccupationListComp from '../../Components/OccupationListComp';
import {colors} from '../../Helpers/colors';
import {styles} from './styles';
//Character Details Screen
const CharacterDetails = ({
  navigation: {pop},
  route: {
    params: {details},
  },
}) => (
  <View style={styles.container}>
    <Header
      title="Character Details"
      showBackButton
      onBackPress={() => {
        pop(); // use for go back
      }}
    />
    {/*====Image Container====*/}
    <ImageBackground
      blurRadius={20}
      source={{uri: details.img}}
      style={[styles.imageCommonStyle, styles.backgroundImage]}>
      <Image
        source={{uri: details.img}}
        style={styles.imageCommonStyle}
        resizeMode="contain"
      />
    </ImageBackground>
    {/*========*/}
    {/*====Name Container====*/}
    <View style={styles.nameContainer}>
      <Text style={styles.name}>{details.name}</Text>
      <Text style={[styles.name, styles.nickName]}>{details.nickname}</Text>
    </View>
    {/*========*/}
    {/** Details Container */}
    <ScrollView contentContainerStyle={styles.scrollViewPadding}>
      <View>
        <Text style={styles.heading}>Occupation</Text>
        <FlatList
          data={details.occupation}
          keyExtractor={(_, i) => i.toString()}
          style={styles.marginTop}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            return <OccupationListComp item={item} />;
          }}
        />
      </View>
      <View style={styles.alignStart}>
        <Text style={styles.heading}>Status</Text>
        <View style={styles.tag}>
          <Text style={[styles.name, styles.labale]}>{details.status}</Text>
        </View>
      </View>
      <View style={styles.alignStart}>
        <Text style={styles.heading}>Season Appearance</Text>
        <View style={styles.tag}>
          <Text style={[styles.name, styles.labale]}>
            {details.appearance.toString().replace(/,/g, ', ')}
          </Text>
        </View>
      </View>
    </ScrollView>
    {/*========*/}
  </View>
);

export default CharacterDetails;
