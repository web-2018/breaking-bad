import {StyleSheet, Dimensions} from 'react-native';
import {colors} from './colors';

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: colors.secondary},
  imageBackground: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
  },
  label: {
    position: 'absolute',
    bottom: 150,
    color: colors.lightGreen,
    textAlign: 'center',
    width: '100%',
    fontSize: 22,
    fontWeight: '700',
    lineHeight: 40,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
});
export default styles;
