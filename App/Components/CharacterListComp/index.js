import React from 'react';
import {Text, TouchableOpacity, Image, Dimensions, View} from 'react-native';
import {colors} from '../../Helpers/colors';
import styles from './styles';
//List Component for Character List
const CharacterListComp = ({item, onPress = () => {}}) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={0.9}
    style={styles.container}>
    <Image source={{uri: item.img}} style={styles.image} resizeMode="cover" />

    <View style={styles.textContainer}>
      <Text style={styles.text}>{item.name}</Text>
    </View>
  </TouchableOpacity>
);

export default CharacterListComp;
