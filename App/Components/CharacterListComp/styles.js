import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../Helpers/colors';
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,.3)',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    marginBottom: 10,
    borderRadius: 8,
    alignItems: 'center',
    overflow: 'hidden',
  },
  image: {height: 200, width: Dimensions.get('window').width / 2 - 30},
  textContainer: {
    bottom: 0,
    alignItems: 'center',
    width: '100%',

    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,.3)',
    paddingVertical: 10,
  },
  text: {
    color: colors.lightGreen,
    fontSize: 18,
    fontWeight: '700',
  },
});
export default styles;
