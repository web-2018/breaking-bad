import React from 'react';
import {Text, View} from 'react-native';
import {colors} from '../../Helpers/colors';
import styles from './styles';
//Occupation List Component
const OccupationListComp = ({item}) => (
  <View style={styles.container}>
    <Text style={styles.lable}>{item}</Text>
  </View>
);

export default OccupationListComp;
