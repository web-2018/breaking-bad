import {StyleSheet} from 'react-native';
import {colors} from '../../Helpers/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginRight: 10,
    borderRadius: 8,
  },
  lable: {fontSize: 15, color: colors.lightGreen, fontWeight: '700'},
});
export default styles;
