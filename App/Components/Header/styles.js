import {StyleSheet} from 'react-native';
import {colors} from '../../Helpers/colors';
const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: colors.secondary,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  backButton: {
    height: 30,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backIcon: {height: 24, width: 24, tintColor: colors.lightGreen},
  title: {
    color: colors.lightGreen,
    fontWeight: '600',
    fontSize: 22,
    textTransform: 'capitalize',
    textAlign: 'center',
    width: '100%',
    position: 'absolute',
    zIndex: -10,
  },
});
export default styles;
