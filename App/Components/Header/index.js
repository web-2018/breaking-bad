import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {colors} from '../../Helpers/colors';
import {images} from '../../Helpers/images';
import styles from './styles';
//Header Component for Customize Header
const Header = ({
  title = '',
  showBackButton = false,
  onBackPress = () => {},
}) => (
  <View style={styles.container}>
    {showBackButton && (
      <TouchableOpacity
        onPress={onBackPress}
        activeOpacity={0.9}
        style={styles.backButton}>
        <Image
          source={images.backIcon}
          style={styles.backIcon}
          resizeMode="contain"
        />
      </TouchableOpacity>
    )}
    <Text style={styles.title}>{title}</Text>
  </View>
);

export default Header;
